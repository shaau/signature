#include<iostream>

using namespace std;

void v()
{
    cout << "      " << endl;
    cout << " \\    / " << endl;
    cout << "  \\  /  " << endl;
    cout << "   \\/   " << endl;
    cout << "      " << endl;
    cout << "      " << endl;
}

void l()
{
    cout << "          " << endl;
    cout << " ||       " << endl;
    cout << " ||       " << endl;
    cout << " ||       " << endl;
    cout << " ||_____| " << endl;
    cout << "          " << endl;
}

void a()
    {
        cout << "         " << endl;
        cout << "         " << endl;
        cout << "   /\\   " << endl;
        cout << "  /--\\  " << endl;
        cout << " /    \\ " << endl;
        cout << "         " << endl;
    }

void d()
{
    cout << "       " << endl;
    cout << "     | " << endl;
    cout << "     | " << endl;
    cout << "   / | " << endl;
    cout << "   \_| " << endl;
    cout << "       " << endl;
}

int main()
{
v();
l();
a();
d();
}
